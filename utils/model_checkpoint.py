
from datetime import datetime
from pathlib import Path
from torch import nn
import pandas as pd
import torch
from model.PlantModel import PlantModel
from utils.utils import check_dir
import config


def save_model_state(model: PlantModel, filename: str, directory: str = Path(config.MODEL_CHECKPOINT_PATH)):
    """
    Save a model state at a given epoch.

    :param filename:  filename to be saved
    :param model: model whose state is to be saved
    :param directory: directory in which states are saved
    """
    state = dict(model=model.state_dict(), model_name=model.model_name)

    filepath = Path(directory) / (filename + ".pth")
    check_dir(directory)
    torch.save(state, filepath)
    print("Model saved at : '{}'".format(filepath))


def load_model_state(filename: str, directory: str = Path(config.MODEL_CHECKPOINT_PATH)):
    """
    Load a model to make inference

    :param filename: filename of the state
    :param directory: directory in which the state has been saved
    :return model: model in eval mode
    """
    filepath = Path(directory) / (filename + ".pth")
    if filepath.exists():
        print("Loading model at : '{}'".format(filepath))
        state = torch.load(filepath)
        model = PlantModel(state["model_name"])
        model.load_state_dict(state['model'])
        print("Model loaded at : '{}'".format(filepath))
        return model
    else:
        print("Model not found at '{}'".format(filepath))


def ensemble_res(path: iter):
    path = list(map(Path, path))
    df_sum = None
    for p in path:
        if df_sum is None:
            df_sum = pd.read_csv(p.as_posix(), index_col=0)
        else:
            df_sum = df_sum.add(pd.read_csv(p.as_posix(), index_col=0))
    df_sum = df_sum.div(len(path))
    df_sum.to_csv("ensemble.csv")
    return df_sum
