from pathlib import Path
import subprocess
import torch
import torch.distributed as dist
import argparse
import pytz
from datetime import datetime, timezone


def get_device(use_cuda: bool = True) -> torch.device:
    if use_cuda:
        if torch.cuda.is_available():
            return get_gpu()
        else:
            print("|!| Warning : You set use_cuda however no GPU seems to be available. CPU is set instead.")
            return get_cpu()
    else:
        return get_cpu()


def get_cpu(): return torch.device("cpu")


def get_gpu(): return torch.device("cuda")


def get_cpu_workers_count():
    return int(subprocess.getoutput(['grep "cpu cores" /proc/cpuinfo | uniq'])[-1])


def collate_fn(batch):
    return tuple(zip(*batch))


def check_dir(directory: str):
    Path(directory).mkdir(parents=True, exist_ok=True)


def is_dist_avail_and_initialized():
    if not dist.is_available():
        return False
    if not dist.is_initialized():
        return False
    return True


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def time_paris_now():
    return datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(tz=pytz.timezone('Europe/Paris'))
