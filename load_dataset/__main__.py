from process_image.ImageSetManager import ImageSetManager
import os
import config
from utils.utils import check_dir
from pathlib import Path
class_list = ["multiple_diseases", "healthy", "rust", "scab"]

IMAGE_TEST_PATH_OUT = config.IMAGE_TEST_PATH
IMAGE_TRAIN_PATH_OUT = config.IMAGE_TRAIN_PATH
check_dir(IMAGE_TEST_PATH_OUT)
check_dir(IMAGE_TRAIN_PATH_OUT)

os.system("cp $(find {}/images/Test_*) {}".format(config.SOURCE_IMAGE_PATH_IN, IMAGE_TEST_PATH_OUT))
os.system("cp $(find {}/images/Train_*) {}".format(config.SOURCE_IMAGE_PATH_IN, IMAGE_TRAIN_PATH_OUT))
os.system("cp $(find {}/test.csv) {}".format(config.SOURCE_IMAGE_PATH_IN, config.SOURCE_IMAGE_PATH_OUT))
os.system("cp $(find {}/train.csv) {}".format(config.SOURCE_IMAGE_PATH_IN, config.SOURCE_IMAGE_PATH_OUT))


image_list = ImageSetManager.Instance().get_image_by_type("test")
mini_test_dir = Path(IMAGE_TEST_PATH_OUT) / "test"
check_dir(mini_test_dir)

for image in image_list:
    os.system("cp {} {}".format(Path(IMAGE_TEST_PATH_OUT) / (image + ".jpg"), mini_test_dir))

for class_type in class_list:
    image_list = ImageSetManager.Instance().get_image_by_type(class_type)
    mini_train_dir = Path(IMAGE_TRAIN_PATH_OUT) / class_type
    check_dir(mini_train_dir)

    for image in image_list:
        os.system("cp {} {}".format(Path(IMAGE_TRAIN_PATH_OUT) / (image + ".jpg"), mini_train_dir))


