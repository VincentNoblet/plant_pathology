#!/bin/bash
set -e
set -u
set -o pipefail

while getopts 'c:' OPTION; do
  case "$OPTION" in
    l)
      echo "linuxconfig"
      ;;

    h)
      echo "h stands for h"
      ;;

    c)
      avalue="$OPTARG"
      echo "Cookie set to $OPTARG"
      virtualenv venv -p /usr/bin/python3
      source venv/bin/activate
      pip install -r requirements.txt
      wget -x --load-cookies $OPTARG https://www.kaggle.com/c/18648/download-all -O source_image/source.zip
      unzip -o source_image/source.zip -d source_image/
      rm source_image/source.zip
      python3 -m load_dataset
      ;;
    ?)
      echo "script usage: $(basename $0) [-l] [-h] [-a somevalue]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"