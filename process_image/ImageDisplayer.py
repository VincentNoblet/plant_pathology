import abc
import cv2
import config
import matplotlib.pyplot as plt


class ImageDisplayer(abc.ABC):
    @abc.abstractmethod
    def begin_session(self):
        pass

    @abc.abstractmethod
    def show(self, matrix, *, window_name, figsize):
        pass

    @abc.abstractmethod
    def close_session(self):
        pass

    def show_image(self, image, **show_params):
        self.begin_session()
        self.show(image.get_numpy(), window_name=image.get_name(), **show_params)
        self.close_session()

    def show_image_list(self, image_list, **show_params):
        self.begin_session()
        list(map(lambda x: self.show(x.get_numpy(), window_name=x.get_name(), **show_params), image_list))
        self.close_session()


class OpenCvDisplayer(ImageDisplayer):
    def begin_session(self):
        pass

    def show(self, matrix, window_name="window_default_name", figsize=None):
        if figsize is None: 
          width = config.IMAGE_WIDTH / 2
          height = config.IMAGE_HEIGHT / 2
        else:
           width = figsize[1]
           height = figsize[0]
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, int(width), int(height))
        cv2.imshow(window_name, matrix)

    def close_session(self):
        cv2.waitKey()
        cv2.destroyAllWindows()


class MatplotlibDisplayer(ImageDisplayer):
    def begin_session(self):
        pass

    def show(self, matrix, window_name="window_default_name", figsize=None):
        if figsize is None: 
          figsize = (40, 28)
        matrix = matrix[:, :, ::-1]
        plt.figure(window_name, figsize=figsize)
        plt.title(window_name)
        plt.imshow(matrix)

    def close_session(self):
        plt.show()


class DefaultDisplayer(OpenCvDisplayer):
    pass