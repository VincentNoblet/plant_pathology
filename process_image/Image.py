import hashlib
from PIL import Image as PILImage
import cv2
import config
import torch
import numpy as np
from pathlib import Path
from process_image.ImageDisplayer import DefaultDisplayer, OpenCvDisplayer, MatplotlibDisplayer


class Image:
    def __init__(self, matrix, name="image_default_name", image_displayer=DefaultDisplayer()):
        self.image_numpy = matrix
        self.name = name
        self.image_displayer = image_displayer

    def get_name(self): return self.name

    def get_tensor(self): return torch.Tensor(self.get_numpy())

    def get_numpy(self): return np.array(self.image_numpy)

    def get_color_image(self, c):
        matrix_color = self.get_numpy()
        channel = [0, 1, 2]
        channel.remove(c)
        for x in channel:
            matrix_color[:, :, x] = 0
        return Image(matrix_color, self.name + "color" + str(c))

    def get_grey_image(self):
        grey_image2D = cv2.cvtColor(self.get_numpy(), cv2.COLOR_BGR2GRAY)
        grey_image2D_T = grey_image2D.T
        grey_image3D = np.array([grey_image2D_T, grey_image2D_T, grey_image2D_T]).T
        return Image(grey_image3D, name=self.name + " grey")

    def show(self, **show_params): self.image_displayer.show_image(self, **show_params)

    def get_hist(self, shape, background_cl=0, fill=True):
        matrix=self.get_numpy()
        background = np.ones((shape[0], shape[1], 3)) * background_cl
        background_list = np.array([np.array(background) for _ in range(3)])

        bins = np.arange(256).reshape(256, 1)
        color = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]

        for ch, col in enumerate(color):
            hist_item = cv2.calcHist([matrix], [ch], None, [256], [0, 255])
            hist_item[0] = 0
            cv2.normalize(hist_item, hist_item, 0, 255, cv2.NORM_MINMAX)
            hist = np.int64(np.around(hist_item))
            pts = np.column_stack((bins, hist))
            pts[:, 0] *= shape[1] // 256
            if fill:
                cv2.fillPoly(background_list[ch], pts=[pts], color=col)
            else:
                cv2.polylines(background, [pts], isClosed=False, color=col, thickness=2)
        if fill:
            background = background_list.sum(axis=0)
        background = np.flipud(background)
        return background

    def position_hist(self, hist):
        matrix = self.get_numpy()
        shape = hist.shape[0:2]
        matrix[-shape[0] - 20: - 20, -shape[1]:, :] = hist
        return matrix

    def get_hist_image(self, background_cl=0, fill=True):
        shape = (300, 256 * 3)
        hist_image = Image(self.position_hist(self.get_hist(shape, background_cl=background_cl, fill=fill)), name=self.name + "_hist")
        return hist_image


class ImageTransform(Image):
    def __init__(self, matrix, normalize, **image_params):
        matrix = np.transpose(matrix, (1, 2, 0))
        matrix = matrix[:, :, ::-1]
        mean = np.array(normalize.mean)
        std = np.array(normalize.std)
        matrix = matrix * std + mean
        super().__init__(matrix, **image_params)


class ImageSource(Image):
    def __init__(self, index, type, **image_params):
        self.file_path = ImageSource.get_image_source_path(index, type)
        if not (Path(self.file_path).exists()):
            raise FileNotFoundError(self.file_path + "does not exists")
        matrix = cv2.imread(self.file_path)
        if matrix.shape[0] > matrix.shape[1]:
            matrix = np.rot90(matrix)
        super().__init__(matrix, name=index, **image_params)

    @staticmethod
    def get_image_source_path(index, type, folder_path=None):
        if folder_path is None:
            if "Train" in index:
                folder_path = config.IMAGE_TRAIN_PATH
            else:
                folder_path = config.IMAGE_TEST_PATH
        else:
            pass
        return folder_path + type + "/" + index + ".jpg"

    def get_metadata(self):
        matrix = PILImage.open(self.file_path)
        extrema = matrix.getextrema()
        md5 = hashlib.md5()
        md5.update(matrix.tobytes())
        meta = {
            'image_id': self.name,
            'hash': md5.hexdigest(),
            'r_min': extrema[0][0],
            'r_max': extrema[0][1],
            'g_min': extrema[1][0],
            'g_max': extrema[1][1],
            'b_min': extrema[2][0],
            'b_max': extrema[2][1],
            'height': matrix.size[1],
            'width': matrix.size[0],
            'format': matrix.format,
            'mode': matrix.mode
        }
        return meta

