import config
from utils.Singleton import Singleton
import os
import pandas as pd
import random
from pathlib import Path
from tqdm.auto import tqdm
from process_image.Image import ImageSource


@Singleton
class ImageSetManager:
    def __init__(self):
        super().__init__()
        self.test_index_list = list(map(lambda x: x.split(".")[0], os.listdir(config.IMAGE_TEST_PATH)))
        random.shuffle(self.test_index_list)
        self.train_index_list = list(map(lambda x: x.split(".")[0], os.listdir(config.IMAGE_TRAIN_PATH)))
        random.shuffle(self.train_index_list)
        self.index_list = self.train_index_list + self.test_index_list
        random.shuffle(self.index_list)
        self.test_label = pd.read_csv(config.TEST_LABEL).sample(frac=1).reset_index(drop=True)
        self.train_label = pd.read_csv(config.TRAIN_LABEL).sample(frac=1).reset_index(drop=True)

    def get_index_list(self): return self.get_test_index_list() + self.get_train_index_list()

    def get_test_index_list(self): return list(self.get_test_label()["image_id"])

    def get_train_index_list(self): return list(self.get_train_label()["image_id"])

    def get_image_by_type(self, type):
        if type == "test":
            test_label = self.get_test_label()
            image = list(test_label["image_id"])
        else:
            train_label = self.get_train_label()
            image = list(train_label[train_label[type] == 1]["image_id"])
        return image

    def get_image_path_type(self, type, path=None):
        train_label = self.get_train_label()
        return list(map(lambda x: ImageSource.get_image_source_path(x, path), train_label[train_label[type] == 1]["image_id"]))

    def get_image_path_test(self, path=None):
        test_label = self.get_test_label()
        return list(map(lambda x: ImageSource.get_image_source_path(x, "test", path), test_label["image_id"]))

    def get_test_label(self): return self.test_label

    def get_train_label(self): return self.train_label

    @staticmethod
    def get_random_index(index, random_state=None):
        index = list(index)
        if random_state is not None:
            return index[random_state]
        else:
            return random.choice(index)

    def get_random_image(self, random_state=None):
        return ImageSource(self.get_random_index(self.get_index_list(), random_state))

    def get_random_image_test(self, random_state=None):
        return ImageSource(self.get_random_index(self.test_label["image_id"], random_state))

    def get_random_image_train(self, random_state=None):
        return ImageSource(self.get_random_index(self.train_label["image_id"], random_state))

    def get_random_image_type(self, type, random_state=None):
        train_label = self.get_train_label()
        if type not in train_label.columns:
            raise ValueError("type shoud be one of the following: " + train_label.columns.drop("image_id"))
        return ImageSource(self.get_random_index(train_label[train_label[type] == 1]["image_id"], random_state), type)

    def load_image(self, index_list, size=None):
        image_id_serie = pd.Series(index_list).iloc[:size]
        image_load_serie = image_id_serie.progress_apply(lambda x: ImageSource(x))
        image_load_serie.name = "image_load"
        return image_load_serie

    def load_image_train(self, size=None):
        tqdm.pandas(desc="Loading train image")
        image_load_serie = self.load_image(self.train_index_list, size=size)
        return image_load_serie

    def get_metadata(self, path=config.STORAGE_PATH):
        return pd.read_csv(path + "metadata.csv")

    def save_metadata(self, path=config.STORAGE_PATH):
        metadata_path = path + "metadata.csv"
        compute = True
        if Path(metadata_path).exists():
          answer = None 
          while answer is None:
            answer = input("Metadata already computed. Force rewriting (y/n)?")
            if answer == "y":
              compute = True
            elif answer == "n":
              compute = False
            else:
              answer = None
        if compute:
          tqdm.pandas(desc="Saving metadata")
          metadata_serie = pd.Series(self.get_index_list()).progress_apply(lambda x: ImageSource(x).get_metadata())
          metadata_frame = pd.DataFrame(list(metadata_serie))
          metadata_frame.to_csv(path + "metadata.csv")
        