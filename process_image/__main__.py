from process_image.ImageDisplayer import OpenCvDisplayer, MatplotlibDisplayer
from process_image.Image import Image, ImageSource
from process_image.ImageSetManager import ImageSetManager
from process_image.ImageMerger import ImageMerger
import numpy
import cv2
import matplotlib.pyplot as plt
import numpy as np
sample_number = 9
random_list = [ImageSetManager.Instance().get_random_image_type("scab", random_state=i) for i in range(sample_number)]
img_src = ImageSource("Test_17")
img = img_src.get_numpy()
gray = img_src.get_grey_image().get_numpy()
blur = cv2.GaussianBlur(gray, (5, 5), 0)
#canny = cv2.Canny(blur, 50, 150, cv) #sick
laplace = cv2.Laplacian(img_src.get_color_image(0).get_grey_image().get_numpy(), ddepth=cv2.CV_8U, ksize=3)
sobelx = cv2.Sobel(gray,cv2.CV_8U,1,0)
sobely = cv2.Sobel(gray,cv2.CV_8U,0,1)

ImageMerger([sobelx, sobely, np.array(np.sqrt(sobely** 2 + sobelx ** 2), np.uint8)], grid=(2, 2)).show(figsize=(2048, 1580))

