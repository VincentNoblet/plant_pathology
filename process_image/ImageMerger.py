from process_image.Image import Image
import numpy as np


class ImageMerger(Image):
    def __init__(self, matrix_list, grid, **image_params):
        matrix = ImageMerger.concatenate(list(matrix_list), grid)
        super().__init__(matrix, **image_params)

    @staticmethod
    def full_matrix_list(matrix_list, grid):
        grid_i, grid_j = grid
        matrix_shape = matrix_list[0].shape
        full_matrix_list = matrix_list
        if grid_i * grid_j < len(matrix_list):
            raise ValueError("List of matrix is too long")
        else:
            blank_grid_length = grid_i * grid_j - len(matrix_list)
            blank_matrix_list = [255 * np.ones(matrix_shape, dtype=np.uint8) for _ in range(blank_grid_length)]
            full_matrix_list += blank_matrix_list
            return np.array(full_matrix_list)

    @staticmethod
    def concatenate(matrix_list, grid):
        grid_i, grid_j = grid
        full_matrix_list = ImageMerger.full_matrix_list(matrix_list, grid)
        concatenate_image = []
        for i in range(grid_i):
            concatenate_image.append(np.concatenate(full_matrix_list[grid_j * i: grid_j * (i + 1)], axis=1))
        concatenate_image = np.concatenate(concatenate_image, axis=0)
        return concatenate_image