# Plant Pathology 
The aim of this repository is to support analysis for any 2D-image classification project. It was built for the [plant pathology 2020] research competition.

> This is not a repository to be used in production. It serves only a purpose of introduction.

# Features

  - Image color histogramm
  - Visualization group by batch
  - Visualization before/after transformation
  - Label analysis
  - Log and store metrics during training process
  - Checkpoint system to make data persistent
  - Black-box interpretation of most important super-pixels
 
# Tools

* Pytorch
* Sklearn
* Pandas
* Torchvision
* Tensorboard
* OpenCV
* efficientnet-pytorch
* Colab
* Plotly
* LIME



# Image description
 
![](utils/readme_image/rust_description.png)
 
![](utils/readme_image/scab.png)

# Image metadata computation

### Computing some metadatas 

![](utils/readme_image/metadata.png)

### Hashing the image to find duplicates

![](utils/readme_image/duplicate.png)

# Show inference

### Showing some interesting output

![](utils/readme_image/pred.png)

### Showing transform and pred in train mode
![](utils/readme_image/pred_label_transform_grid.png)

### Showing transform and pred in evaluation mode
![](utils/readme_image/valid_pred_mitigate.png)

### Interpret important features

![](utils/readme_image/LIME.png)

# Training management

### Custom loader

![](utils/readme_image/custom_loader.png)

### Metrics follow-up

![](utils/readme_image/metrics.png)

### Checkpoint prediction

![](utils/readme_image/checkpoint.png)



   [plant pathology 2020]: <https://www.kaggle.com/c/plant-pathology-2020-fgvc7/overview>