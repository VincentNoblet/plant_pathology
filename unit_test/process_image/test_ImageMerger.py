import unit_test
from unit_test import TestCase
from process_image.ImageMerger import ImageMerger
from process_image.Image import Image
import numpy as np

class TestImageMerger(TestCase):
    image_list = [Image(np.ones([2, 2]) * i) for i in range(1, 4)]
    merger = ImageMerger(image_list)

    def test_get_grid_image(self):
        image_out = self.merger.get_grid_image(2, 2)
        image_out_expected = np.array([[1, 1, 2, 2],
                                       [1, 1, 2, 2],
                                       [3, 3, 255, 255],
                                       [3, 3, 255, 255]])
        self.assertTrue(np.array_equal(image_out.get_numpy(), image_out_expected))

    def test_small_grid(self):
        with self.assertRaises(ValueError):
            self.merger.get_grid_image(2, 1)
