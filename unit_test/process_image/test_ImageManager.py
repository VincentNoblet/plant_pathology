from unit_test import TestCase
from process_image.ImageSetManager import *


class TestImageSetManager(TestCase):
    image_set_manager = ImageSetManager.Instance()

    def test_get_test_index_list(self):
        self.assertEqual(1821, len(self.image_set_manager.get_test_index_list()))

    def test_get_train_index_list(self):
        self.assertEqual(1821, len(self.image_set_manager.get_train_index_list()))

    def test_get_test_label(self):
        test_label = self.image_set_manager.get_test_label()
        self.assertIsInstance(test_label, pd.DataFrame)
        self.assertEqual((1821, 1), test_label.shape)
        set_test_label_id = set(ImageSetManager.Instance().get_test_label()["image_id"])
        set_test_index_list = set(self.image_set_manager.get_test_index_list())
        self.assertEqual(set_test_index_list, set_test_label_id)

    def test_get_train_label(self):
        train_label = self.image_set_manager.get_train_label()
        self.assertIsInstance(train_label, pd.DataFrame)
        self.assertEqual((1821, 5), train_label.shape)
        set_train_label_id = set(ImageSetManager.Instance().get_train_label()["image_id"])
        set_train_index_list = set(self.image_set_manager.get_train_index_list())
        self.assertEqual(set_train_label_id, set_train_index_list)

    def test_get_random_index(self):
        index = list(range(0, 100))
        random.shuffle(index)
        id_1 = self.image_set_manager.get_random_index(index, random_state=42)
        id_2 = self.image_set_manager.get_random_index(index, random_state=42)
        self.assertEqual(id_1, id_2)
        id_3 = self.image_set_manager.get_random_index(index, random_state=2)
        self.assertNotEqual(id_2, id_3)

    def test_get_random_image_test(self):
        image = self.image_set_manager.get_random_image_test()
        self.assertIsInstance(image, ImageSource)
        self.assertTrue(image.name in list(self.image_set_manager.get_test_label()["image_id"]))

    def test_get_random_image_train(self):
        image = self.image_set_manager.get_random_image_train()
        self.assertIsInstance(image, ImageSource)
        self.assertTrue(image.name in list(self.image_set_manager.get_train_label()["image_id"]))

    def test_load_image_train(self):
        image_frame = self.image_set_manager.load_image_train(3)
        self.assertIsInstance(image_frame, pd.Series)
        self.assertIsInstance(image_frame.iloc[0], ImageSource)

    def test_get_random_image_type(self):
        image = self.image_set_manager.get_random_image_test()
        self.assertIsInstance(image, ImageSource)