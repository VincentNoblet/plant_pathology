from unit_test import TestCase
from process_image.Image import Image, ImageSource
import numpy as np


class TestImageSource(TestCase):
    image = ImageSource("Train_1569")

    def test_image_lookup(self):
        ImageSource("Train_1569")
        with self.assertRaises(FileNotFoundError):
            ImageSource("XXX")

    def test_get_matrix(self):
        mat = self.image.get_numpy()
        self.assertEqual((1365, 2048, 3), mat.shape)

    def test_get_index(self):
        index = self.image.get_name()
        self.assertEqual("Train_1569", index)


class TestImage(TestCase):
    image = Image(np.ones((2, 2, 3)), name="default")

    def test_get_matrix(self):
        mat = self.image.get_numpy()
        self.assertTrue(np.array_equal(np.ones((2, 2, 3)), mat))

    def test_get_index(self):
        index = self.image.get_name()
        self.assertEqual("default", index)