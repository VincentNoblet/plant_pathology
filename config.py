ROOT_PATH = "/content/drive/My Drive/PlantPathology/"
SOURCE_IMAGE_PATH_IN = "source_image"
SOURCE_IMAGE_PATH_OUT = ROOT_PATH + "source_image/"
STORAGE_PATH = ROOT_PATH + "data/"
LOG_PATH = STORAGE_PATH + "log/"
TRAIN_CHECKPOINT_PATH = STORAGE_PATH + "train_checkpoint/"
MODEL_CHECKPOINT_PATH = STORAGE_PATH + "model_checkpoint"
SUBMISSION_PATH = STORAGE_PATH + "submission/"

IMAGE_TEST_PATH = SOURCE_IMAGE_PATH_OUT + "Test/"
IMAGE_TRAIN_PATH = SOURCE_IMAGE_PATH_OUT + "Train/"
TEST_LABEL = SOURCE_IMAGE_PATH_OUT + "test.csv"
TRAIN_LABEL = SOURCE_IMAGE_PATH_OUT + "train.csv"
IMAGE_WIDTH = 2048
IMAGE_HEIGHT = 1365
SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
IMAGE_ASPECT_RATIO = IMAGE_HEIGHT / IMAGE_WIDTH

class_to_idx = {'0': 0, '1': 1}
class_to_idx_inv = dict(map(lambda x: (x[1], x[0]), class_to_idx.items()))
