import torchvision
import matplotlib.pyplot as plt
import numpy as np
from process_image.Image import Image
import torch

def show_batch(batch, normalize):
    tensor = torchvision.utils.make_grid(batch)
    return show_tensor(tensor, normalize)

def transpose_channel_first(matrix):
    return np.transpose(matrix, (2, 0, 1))

def transpose_channel_last(matrix):
    return np.transpose(matrix, (1, 2, 0))

def transpose_batch_channel_first(matrix):
    return np.transpose(matrix, (0, 3, 1, 2))

def transpose_batch_channel_last(matrix):
    return np.transpose(matrix, (0, 2, 3, 1))


def input_to_image(matrix, normalize=None, uint8=True, transpose_last=True):
    if isinstance(matrix, torch.Tensor):
        matrix = matrix.numpy()
    if transpose_last:
        matrix = transpose_channel_last(matrix)
    if normalize is not None:
        mean = np.array(normalize.mean)
        std = np.array(normalize.std)
        matrix = std * matrix + mean
        matrix = np.clip(matrix, 0, 1)
    if uint8:
        matrix = np.array(255 * matrix, np.uint8)
    return matrix


def show_tensor(tensor, normalize):
    plt.figure(figsize=(20, 10))
    matrix = input_to_image(tensor, normalize)
    plt.imshow(matrix)
    plt.pause(0.001)
    return matrix