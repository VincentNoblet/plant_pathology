import matplotlib.pyplot as plt
from PIL import Image, ImageEnhance, ImageOps
import numpy as np
import random
import torchvision
from transform.standardization import LandscapeStandard
import albumentations as A
import torchvision
from cv2 import BORDER_REFLECT
from transform.policy import ImageNetPolicy, _IMAGENET_PCA
import torch

from torchvision.transforms import (
    Resize,
    CenterCrop,
    RandomHorizontalFlip,
    RandomErasing,
    ToTensor,
    Normalize,
    Compose,
)


class DatasetTransformsAutoAug:
    def __init__(self, train: bool, resize_size: tuple, crop_size: tuple, **policy_rule):
        self.transforms = []
        self.set_transform(LandscapeStandard())
        self.crop_size = crop_size
        self.set_transform(CenterCrop(crop_size))
        self.resize_size = resize_size
        self.set_transform(Resize(resize_size))
        if train:
            self.set_train_transform(**policy_rule)
        self.set_transform(ToTensor())
        self.set_transform(self.get_normalization())

    def set_transform(self, transform):
        self.transforms.append(transform)

    def set_transform_list(self, transform):
        self.transforms += transform

    def set_train_transform(self, **policy_args):
        self.set_transform(RandomHorizontalFlip())
        self.set_transform(ImageNetPolicy())
        if "cutout" in policy_args and policy_args["cutout"]:
            self.transforms.append(RandomErasing(p=0.4))

        if "lighting" in policy_args and policy_args["lighting"]:
            self.transforms += [
                Lighting(0.1, _IMAGENET_PCA['eigval'], _IMAGENET_PCA['eigvec']),
            ]

    def get_normalization(self):
        return Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )

    def get_compose(self):
        return Compose(self.transforms)

    def __call__(self, x):
        return Compose(self.transforms)(x)


class DatasetTransformsAlbumentation(DatasetTransformsAutoAug):
    def __init__(self, train: bool, resize_size: tuple, crop_size: tuple, **policy_rule):
        self.transforms = []
        self.set_transform(LandscapeStandard())
        self.crop_size = crop_size
        self.set_transform(A.CenterCrop(crop_size))
        self.resize_size = resize_size
        self.set_transform(A.Resize(resize_size))
        if train:
            self.set_train_transform()
        self.set_transform(ToTensor())
        self.set_transform(self.get_normalization())

    def __init__(self, train: bool, resize_size: tuple, crop_size: tuple):
        super().__init__(train, resize_size, crop_size)

    def set_train_transform(self):
            self.set_transform_list([
                A.HorizontalFlip(p=0.5),
                A.VerticalFlip(p=0.5),
                A.Rotate(p=0.5, border_mode=BORDER_REFLECT, value=0),

                # Pixels
                A.OneOf([
                    A.IAAEmboss(p=1.0),
                    A.IAASharpen(p=1.0),
                    A.Blur(p=1.0),
                ], p=0.5),

                # Affine
                A.OneOf([
                    A.ElasticTransform(p=1.0),
                    A.IAAPiecewiseAffine(p=1.0)
                ], p=0.5),
            ])


class Lighting(object):
    """Lighting noise(AlexNet - style PCA - based noise)"""

    def __init__(self, alphastd, eigval, eigvec):
        self.alphastd = alphastd
        self.eigval = torch.Tensor(eigval)
        self.eigvec = torch.Tensor(eigvec)

    def __call__(self, img):
        if self.alphastd == 0:
            return img

        alpha = img.new().resize_(3).normal_(0, self.alphastd)
        rgb = self.eigvec.type_as(img).clone() \
            .mul(alpha.view(1, 3).expand(3, 3)) \
            .mul(self.eigval.view(1, 3).expand(3, 3)) \
            .sum(1).squeeze()

        return img.add(rgb.view(3, 1, 1).expand_as(img))
