from PIL import Image as PILImage
import config


class LandscapeStandard(object):
  def __call__(self, sample):
    image = sample
    if image.size[0] == config.IMAGE_HEIGHT:
      image = image.transpose(PILImage.ROTATE_270)
    return image
