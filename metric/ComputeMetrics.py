import config
import numpy as np
import torch
from sklearn.metrics import roc_curve, auc
import pandas as pd


class ComputeMetrics(object):
    def __init__(self, n_classes=2):
        self.n_classes = n_classes
        self.output = torch.FloatTensor()
        self.target = torch.LongTensor()
        self.loss = torch.FloatTensor()

    def update(self, output: torch.Tensor, target: torch.Tensor, loss: torch.Tensor):
        output = output.detach().cpu()
        target = target.detach().cpu()
        loss = loss.detach().cpu().unsqueeze(0)

        self.output = torch.cat([self.output, output])
        self.target = torch.cat([self.target, target])
        self.loss = torch.cat([self.loss, loss])

    def get_metric_dict(self) -> dict:
        output = torch.nn.functional.softmax(self.output, dim=1).numpy()
        target = self.target.numpy().astype(np.uint8)
        loss = self.loss.numpy()
        metric_dict = dict()
        metric_dict["accuracy"] = (output.argmax(axis=1) == target).mean()
        metric_dict["loss"] = np.mean(loss)
        target_dummy = np.array(pd.get_dummies(target).reindex(columns=config.class_to_idx.values(), fill_value=0))
        roc_auc = dict()
        for i in range(self.n_classes):
            classname = config.class_to_idx_inv[i] + "_auc"
            fpr, tpr, _ = roc_curve(target_dummy[:, i], output[:, i])
            roc_auc[classname] = auc(fpr, tpr)
        metric_dict["mean_auc"] = np.mean(list(roc_auc.values()))
        metric_dict.update(roc_auc)


        return metric_dict


def print_result_table(train_metric: dict, test_metric: dict):
    headers = ["metric", "train", "test", "diff"]
    data = sorted(
        [(k, v_train, v_test, v_train/v_test)
         for (k, v_train), (_, v_test)
         in zip(train_metric.items(), test_metric.items())]
    )
    print(tabulate(data, headers=headers))