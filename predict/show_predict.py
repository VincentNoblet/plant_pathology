import pandas as pd
import numpy as np
import torch
from transform.show_transform import input_to_image
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import config

RED = np.array([255, 0, 0])
GREEN = np.array([0, 255, 0])
WHITE = np.array([255, 255, 255])
class_to_idx_inv = config.class_to_idx_inv


def get_predict_bar(fig, tensor: np.array, label: int, prob: list, i: int, j: int, opt_tensor: np.array = None):

    prob_df = pd.DataFrame([prob], index=["prob"], columns=class_to_idx_inv.values()).rename_axis("class_name", axis=1).T
    label = int(label)

    color_list = [GREEN if idx == label else RED for idx in class_to_idx_inv.keys()]
    class_description = pd.DataFrame(dict(
        idx=list(class_to_idx_inv.keys()),
        description=["Healthy", "Multiple diseases", "Rust", "Scab"],
        color=color_list
    ), index=class_to_idx_inv.values()).rename_axis("class_name", axis=0)
    class_description = pd.merge(class_description, prob_df, on="class_name")
    class_description["color"] += class_description["prob"].apply(lambda x: (1 - x - 0.3) * WHITE)
    class_description["color"] = class_description["color"].apply(lambda x: np.array(np.clip(x, 0, 255), dtype=np.uint8))

    fig.add_trace(go.Image(z=tensor), row=i, col=3 * j - 2)
    if opt_tensor is not None:
        fig.add_trace(go.Image(z=opt_tensor), row=i, col=3 * j - 1)
    fig.add_trace(go.Bar(x=class_description["description"], y=class_description["prob"],
                         marker_color=list(map(lambda x: 'rgb' + str(tuple(x)), class_description["color"]))), row=i, col=3 * j)
    fig.update_yaxes(range=[0, 1], row=i, col=3*j)
    return fig


"""

from lime import lime_image
from transform.show_transform import *

import matplotlib.pyplot as plt
from skimage.segmentation import mark_boundaries

model = load_model_state("efficientnetnsb5_May04_23-38_ep17.pth", device)
def pred_lime(batch):
    batch = transpose_batch_channel_first(batch)
    batch = torch.Tensor(batch)
    prob = pred.get_prediction_from_batch(batch)
    return prob.detach().cpu().numpy()

explainer = lime_image.LimeImageExplainer()
normalization = transform_valid.get_normalization()
pred = Predict(model, utils.get_cpu())


id = 2
num_features = 50
iter_loader = iter(train_loader)
batch, label = next(iter_loader)
image = transpose_channel_last(batch[id].numpy())
explanation = explainer.explain_instance(image,
                                         pred_lime, # classification function
                                         top_labels=1,
                                         num_features=num_features,
                                         labels=[1 if i == label[id] else 0 for i in range(4)],
                                         num_samples=5) # number of images that will be sent to classification functio


temp, mask = explanation.get_image_and_mask(explanation.top_labels[0], positive_only=False, num_features=num_features, hide_rest=False)
img_boundry1 = mark_boundaries(input_to_image(temp, normalize=normalization, uint8=False, transpose_last=False), mask)
plt.imshow(img_boundry1)
"""