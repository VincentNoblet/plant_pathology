import torch
from efficientnet_pytorch import EfficientNet
from utils import utils
import config
from predict.show_predict import get_predict_bar
from utils.exec_time import timeit
from tqdm import tqdm
from torchvision.transforms import Resize
from transform.show_transform import input_to_image, transpose_channel_last, transpose_channel_first
from plotly.subplots import make_subplots

from predict.tta import d4_image2label


class Predict:
    def __init__(self, model, device, class_to_idx=config.class_to_idx):
        self.device = device
        self.model = model.to(self.device).eval()
        self.class_to_idx = class_to_idx
        self.predict_f = self.predict

    def predict(self, x):
        return self.model(x)

    def get_prediction_from_loader(self, loader):
        with torch.no_grad():
            output = []
            for input_, _ in tqdm(loader, desc="Prediction from loader"):
                output.append(self.get_prediction_from_batch(input_))
        return torch.cat(output, axis=0)

    def set_tta(self):
        self.predict_f = lambda x: d4_image2label(self.model, x)

    @torch.no_grad()
    def get_prediction_from_batch(self, input_):
        input_ = input_.to(self.device)
        output_ = self.predict_f(input_)
        return torch.softmax(output_, dim=1)

    def get_prediction_from_one(self, tensor):
        input_ = tensor.unsqueeze(0)
        input_ = input_.to(self.device)
        output_ = self.get_prediction_from_batch(input_)
        return output_[0]

    def show_predict_from_one(self, tensor, label, normalization, original_tensor):
        prob = self.get_prediction_from_one(tensor).detach().cpu().numpy()
        fig = make_subplots(1, 3 * 1)
        fig = get_predict_bar(fig, input_to_image(tensor, normalization), label, prob, 1, 1, transpose_channel_last(original_tensor))
        fig.update_layout(title_text="DenseNet Predictions", showlegend=False)
        fig.show()

    def show_predict_from_list(self, tensor, label, normalization, grid_j, original_tensor):
        n = len(label)
        fig = make_subplots((n - 1) // grid_j + 1, 3 * grid_j)
        for c in range(len(label)):
            prob = self.get_prediction_from_one(tensor[c]).cpu().detach().numpy()
            fig = get_predict_bar(fig, input_to_image(tensor[c], normalization), label[c], prob, (c // grid_j) + 1, (c % grid_j) + 1, transpose_channel_first(original_tensor[c]))
        fig.update_layout(title_text="DenseNet Predictions", showlegend=False)
        return fig




