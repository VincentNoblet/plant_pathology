from pathlib import Path

import pytz

from predict.make_prediction import Predict
from torchvision import datasets
from torch.utils.data import DataLoader
import config
from utils import utils
import numpy as np
import pandas as pd
import datetime

cpu_workers_count = utils.get_cpu_workers_count()


class Submit(Predict):
    def __init__(self, transform, model, device, batch_size, class_to_idx=config.class_to_idx, storage_path=config.STORAGE_PATH):
        super().__init__(model, device)
        self.test_dataset = datasets.ImageFolder(config.IMAGE_TEST_PATH, transform=transform)
        self.class_to_idx = class_to_idx
        self.storage_path = storage_path
        self.test_loader = DataLoader(self.test_dataset, batch_size=batch_size, shuffle=False, num_workers=cpu_workers_count)

    def get_submission(self):
        sub = self.get_prediction_from_loader(self.test_loader).cpu().numpy()
        return pd.DataFrame(sub, columns=self.class_to_idx, index=self.get_id()).rename_axis("image_id", axis=0)

    def save_submission(self, name):
        sub_df = self.get_submission()
        directory = self.storage_path + "submission/"
        utils.check_dir(directory)
        filename = "{}.csv".format(name)
        filepath = Path(directory) / filename
        sub_df.to_csv(filepath)
        print("submission made at: {}".format(filepath))


    def get_id(self):
        sample_id = np.array(self.test_dataset.samples)[:, 0]
        return list(map(lambda x: x.split("/")[-1].split(".")[0], sample_id))
