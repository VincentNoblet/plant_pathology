from train.__main__ import TaskTrain, TaskTrainInvalid
import os
import numpy as np
import pandas as pd
from predict.make_submission import Submit
import config
from pathlib import Path


def predict(dir, sub_name):
    df_list = []
    for i, file in enumerate(os.listdir(dir)):
      df = pd.read_csv(Path(dir) / file, index_col="image_id")
      df = df.sort_index(axis=1)
      df = df.sort_index(axis=0)
      df_list.append(df.values)
      index = df.index
      columns = df.columns

    values = np.mean(df_list, axis=0)
    df = pd.DataFrame(values, index=index, columns=columns)
    df.to_csv(Path(config.SUBMISSION_PATH) / "{}.csv".format(sub_name))


