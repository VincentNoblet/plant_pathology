import torch
from torch.utils.data.dataloader import DataLoader
from metric.metric_logger import MetricLogger, SmoothedValue
from metric.ComputeMetrics import ComputeMetrics
import argparse
from pathlib import Path
from typing import Union

import config
import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from metric.metric_logger import MetricLogger, SmoothedValue
from metric.ComputeMetrics import print_result_table
from utils.utils import get_device
from utils.model_checkpoint import save_model_state, load_model_state


def forward_one_epoch(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device,
                      train_mode: bool, epoch: int = None, optimizer: torch.optim.Optimizer = None,
                      print_freq: int = None, log_path: str = config.LOG_PATH + "batch/") -> dict:
    """
    Train a model on one epoch and compute metrics

    :param model: model to be forwarded
    :param criterion: loss function
    :param loader: image loader to browse
    :param device: device to perform the forward on
    :param train_mode: training mode or evaluate mode
    :param epoch: current epoch (only for train_mode=True)
    :param optimizer: optimizer rule (only for train_mode=True)
    :param print_freq: frequency of metrics computation (only for train_mode=True)
    :param log_path: log path
    :return: dict of metrics
    """
    metric = ComputeMetrics(n_classes=2)
    if train_mode:
        model.train()
        batch_logger = MetricLogger(delimiter=" | ", writer=SummaryWriter(log_path))
        batch_logger.add_meter('batch_loss', SmoothedValue(window_size=print_freq, fmt="{value:.4f} (win_avg: {avg:.4f})"))
        header = 'Epoch: [{}]'.format(epoch)
        loader = batch_logger.log_every_batch(loader, print_freq, epoch, header)
    else:
        model.eval()

    model = model.to(device)
    for i_batch, (image_batch, label_batch) in enumerate(loader):
        image_batch = image_batch.to(device)
        label_batch = label_batch.to(device)

        output_batch = model(image_batch)
        loss = criterion(output_batch, label_batch)
        metric.update(output_batch, label_batch, loss)

        if train_mode:
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            batch_logger.update(batch_loss=loss)

    metric_dict = metric.get_metric_dict()
    return metric_dict


@torch.no_grad()
def evaluate(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device) -> dict:
    """
    Evaluate model and compute metrics

    :param model: model to be evaluated
    :param criterion: loss function
    :param loader: image loader browse for evaluation
    :param device: device to perform the evaluation on
    :return: dict of metrics
    """
    return forward_one_epoch(model, criterion, loader, device, False)


def train_one_epoch(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device,
                    epoch: int, optimizer: torch.optim.Optimizer, print_freq: int, log_path: str) -> dict:
    """
    Train a model on one epoch and compute metrics

    :param model: model to be trained
    :param criterion: loss function
    :param loader: image loader to browse for training
    :param device: device to perform the training on
    :param epoch: current epoch
    :param optimizer: optimizer rule only for train_mode=True
    :param print_freq: frequency of metrics computation only for train_mode=True
    :param log_path: log path of train batch only for train_mode=True
    :return: dict of metrics
    """
    return forward_one_epoch(model, criterion, loader, device, True, epoch, optimizer, print_freq, log_path)


@torch.no_grad()
def mock_train_one_epoch(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device,
                    epoch: int, optimizer: torch.optim.Optimizer, print_freq: int, log_path: str) -> dict:
    return mock_forward_one_epoch(model, criterion, loader, device, True, epoch, optimizer, print_freq, log_path)

@torch.no_grad()
def mock_evaluate(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device) -> dict:
    return mock_forward_one_epoch(model, criterion, loader, device, False)


@torch.no_grad()
def mock_forward_one_epoch(model: nn.Module, criterion: torch.nn.modules.loss, loader: DataLoader, device: torch.device,
                      train_mode: bool, epoch: int = None, optimizer: torch.optim.Optimizer = None,
                      print_freq: int = None, log_path: str = config.LOG_PATH + "batch/") -> dict:

    metric = ComputeMetrics(n_classes=4)
    if train_mode:
        model.train()
        batch_logger = MetricLogger(delimiter=" | ", writer=SummaryWriter(log_path))
        header = 'Epoch: [{}]'.format(epoch)
        loader = batch_logger.log_every_batch(loader, print_freq, epoch, header)
    else:
        model.eval()

    model = model.to(device)
    for i_batch, (image_batch, label_batch) in enumerate(loader):
        image_batch = image_batch.to(device)
        label_batch = label_batch.to(device)

        output_batch = model(image_batch)
        loss = criterion(output_batch, label_batch)
        metric.update(output_batch, label_batch, loss)

        if train_mode:
            batch_logger.update(batch_loss=loss)

    metric_dict = metric.get_metric_dict()
    return metric_dict
