import datetime
from copy import deepcopy
import torch
from torch import nn
from torch.utils.data import Subset, DataLoader
from torchvision import datasets
import pandas as pd
from model.PlantModel import PlantModel
import numpy as np
from sklearn.model_selection import StratifiedKFold
from efficientnet_pytorch import EfficientNet
import pandas as pd
import numpy as np
import torch
import skimage
import lime
from transform.show_transform import input_to_image
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from torchvision.transforms import ToTensor, Compose, Resize
from progressbar import ProgressBar
from transform.show_transform import *

import config
from transform.autoaugment import DatasetTransformsAutoAug, DatasetTransformsAlbumentation
from train.cross_validation import CrossValidationManager
from train.train_management import train_one_epoch, evaluate
from optimizer.Ranger import Ranger
from train.train_management import mock_train_one_epoch, mock_evaluate
from predict.make_prediction import Predict
from predict.show_predict import get_predict_bar
from utils import utils
from utils.model_checkpoint import save_model_state, load_model_state
from train import learning_rate as utils_learning_rate
import imp
import train

device, cpu, cpu_workers_count = utils.get_device(), utils.get_cpu(), utils.get_cpu_workers_count()

# LEARNING
epochs = np.arange(1, 3)
batch_size = 1
criterion = nn.CrossEntropyLoss()
learning_rate = utils_learning_rate.adapt_logisitic(epochs, start=10 ** (-5), end=10 ** (-6))

# DATASET
dataset = datasets.ImageFolder(config.IMAGE_TRAIN_PATH)
kfold = CrossValidationManager(dataset, StratifiedKFold).get_n_splits(n_splits=5, random_state=42)[:1]
dataload_params = dict(batch_size=batch_size, num_workers=cpu_workers_count)

# TRANSFORM
crop_size = (config.IMAGE_HEIGHT * 0.6, config.IMAGE_WIDTH * 0.6)
resize_size = 512, 512
transform_train = DatasetTransformsAutoAug(train=True, resize_size=resize_size, crop_size=crop_size)
transform_valid = DatasetTransformsAutoAug(train=False, resize_size=resize_size, crop_size=crop_size)

for i_fold, (train_id, valid_id) in enumerate(kfold):
    np.random.shuffle(train_id)
    np.random.shuffle(valid_id)
    model = PlantModel(backbone_name="efficientnetnsb1", pretrained=True, finetune=True, num_classes=4, layer_freezed=3)
    optimizer = Ranger(model.parameters(), lr=1e-5)
    e = -1
    train_dataset, valid_dataset = Subset(deepcopy(dataset), train_id), Subset(deepcopy(dataset), valid_id)
    dataset_no_transform = Subset(deepcopy(dataset), train_id)
    train_dataset.dataset.transform, valid_dataset.dataset.transform,  = transform_train, transform_valid
    dataset_no_transform.dataset.transform = Compose([Resize((512, 512)), ToTensor()])

    train_loader = DataLoader(train_dataset, shuffle=False, **dataload_params)
    valid_loader = DataLoader(valid_dataset, shuffle=False, **dataload_params)
    no_transform_loader = DataLoader(dataset_no_transform, shuffle=False, **dataload_params)

train_batch, train_label = next(iter(train_loader))

normalization = transform_valid.get_normalization()
pred = Predict(model, device, tta=True)
