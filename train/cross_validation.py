import numpy as np
import pandas as pd
from collections import Counter


class CrossValidationManager:
    def __init__(self, dataset, kfold):
        self.dataset = dataset
        self.kfold = kfold

    def get_class_proportion(self):
        count_by_class = pd.Series({key: Counter(self.dataset.targets)[value] for key, value in self.dataset.class_to_idx.items()})
        return count_by_class

    def get_weight_error(self):
        pass

    def get_n_splits(self, n_splits, random_state=42):
        y = self.dataset.targets
        fold = []
        for train, valid in self.kfold(n_splits=n_splits, shuffle=True, random_state=random_state).split(np.zeros(len(y)), y):
            fold.append([])
            fold[-1].append(train)
            fold[-1].append(valid)
        return fold
