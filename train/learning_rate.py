import numpy as np


def adapt_logisitic(t, *, start, end, inflection=10):
    x = (t - t.min()) / (t.max() - t.min())
    x = inflection * x - inflection / 2
    return start + (end - start) / (1 + np.exp(-x))
