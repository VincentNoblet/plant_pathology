import datetime
from copy import deepcopy

import dill
import pytz
import os
import torch
from torch import nn
from torch.utils.data import Subset, DataLoader
from torchvision import datasets
import pandas as pd
from model.PlantModel import PlantModel
import numpy as np
from sklearn.model_selection import StratifiedKFold
from efficientnet_pytorch import EfficientNet
from metric.metric_logger import SmoothedValue, MetricLogger
from metric.ComputeMetrics import ComputeMetrics
from torch.utils.tensorboard import SummaryWriter
import config
from transform.autoaugment import DatasetTransformsAutoAug, DatasetTransformsAlbumentation
from train.cross_validation import CrossValidationManager
from train.train_management import train_one_epoch, evaluate
from optimizer.Ranger import Ranger
from train.train_management import mock_train_one_epoch, mock_evaluate
from predict.make_prediction import Predict
from predict.show_predict import get_predict_bar
from utils.utils import check_dir
from utils import utils
from utils.model_checkpoint import save_model_state, load_model_state
from train import learning_rate as utils_learning_rate
from predict.make_submission import Submit
from pathlib import Path
import d6tflow
import luigi
d6tflow.data = config.TRAIN_CHECKPOINT_PATH


class TaskTrain(d6tflow.tasks.TaskPickle):
    name = luigi.parameter.Parameter()

    def __init__(self, name, *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        check_dir(config.TRAIN_CHECKPOINT_PATH)
        check_dir(config.STORAGE_PATH)
        check_dir(config.LOG_PATH)
        check_dir(config.SUBMISSION_PATH)
        self.log_epoch_path = config.LOG_PATH + self.name + "/epoch/"
        self.log_batch_path = config.LOG_PATH + self.name + "/batch/"
        self.device = utils.get_gpu()
        self.cpu_workers_count = utils.get_cpu_workers_count()
        self.epochs = np.arange(1, 41)
        # LEARNING

        self.criterion = nn.CrossEntropyLoss(weight=torch.Tensor([1, 1, 1, 1]))
        self.learning_rate = utils_learning_rate.adapt_logisitic(np.arange(1, 41), start=8e-4, end=1e-6, inflection=5)
        self.set_model_optimizer(PlantModel(backbone_name="efficientnetnsb5", pretrained=True, finetune=True, num_classes=4, layer_freezed=3))

        # DATASET
        self.dataset = datasets.ImageFolder(config.IMAGE_TRAIN_PATH)
        self.random_state = 40
        self.ifold = 0

        self.train_dataload_params = dict(batch_size=8, shuffle=False, num_workers=self.cpu_workers_count)
        self.valid_dataload_params = dict(batch_size=40, shuffle=False, num_workers=self.cpu_workers_count)

        # TRANSFORM
        self.crop_size = (config.IMAGE_HEIGHT * 0.8, config.IMAGE_WIDTH * 0.8)
        self.resize_size = 512, 512
        self.train_transform = DatasetTransformsAutoAug(train=True, resize_size=self.resize_size, crop_size=self.crop_size)
        self.valid_transform = DatasetTransformsAutoAug(train=False, resize_size=self.resize_size, crop_size=self.crop_size)

        #TRAIN MANAGEMENT
        self.running_i_e = 0
        self.max_score = 0
        self.batch_print_freq = 40
        self.epoch_print_freq = 1
        self.dump_freq = 1
        self.save_model_freq = 50
        self.submit_checkpoint_list = [i for i in range(25, 30)]

    def set_model_optimizer(self, model):
        self.model = model
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate[0])

    def get_kfold(self): return CrossValidationManager(self.dataset, StratifiedKFold).get_n_splits(n_splits=5, random_state=self.random_state)[self.ifold]

    @staticmethod
    def get_pkl_filepath(name): return Path(config.TRAIN_CHECKPOINT_PATH) / (name + ".pkl")

    def get_checkpoint_filepath(self): return Path(config.TRAIN_CHECKPOINT_PATH) / (self.name + "_checkpoint")

    def reset(self):
        self.running_i_e = 0
        self.max_score = 0
        os.system("rm -r '{}'".format(Path(config.MODEL_CHECKPOINT_PATH) / self.name))

    def dump(self):
        dill.dump(self, open(self.get_pkl_filepath(self.name), "wb"))
        self.save_checkpoint()

    @staticmethod
    def load(name):
        train_task = dill.load(open(TaskTrain.get_pkl_filepath(name), "rb"))
        train_task.load_checkpoint()
        print(train_task.name, " loaded")
        return train_task

    def submit(self, model, name, tta=False):
        submit = Submit(self.valid_transform, model, self.device, 40)
        if tta:
            submit.set_tta()
        submit.save_submission(Path(self.name) / name)

    def evaluate(self, model):
        return evaluate(model, self.criterion, self.valid_loader, self.device)

    def save_checkpoint(self):
        checkpoint_dict = dict(model=self.model.state_dict(), optimizer=self.optimizer.state_dict())
        torch.save(checkpoint_dict, self.get_checkpoint_filepath())

    def load_checkpoint(self):
        checkpoint_dict = torch.load(self.get_checkpoint_filepath())
        self.model.load_state_dict(checkpoint_dict["model"])
        self.optimizer.load_state_dict(checkpoint_dict["optimizer"])

    def save_model_checkpoint(self, result_name, result):
        result = str(round(result, 4))
        ep = self.epochs[self.running_i_e]
        model_name = self.model.model_name
        filename = "{model_name}_ep{ep}_{result_name}{result}".format(model_name=model_name, ep=ep, result_name=result_name, result=result)
        save_model_state(self.model, filename, directory=Path(config.MODEL_CHECKPOINT_PATH) / self.name)

    def submit_checkpoint(self, result_name, result):
        result = str(round(result, 4))
        ep = self.epochs[self.running_i_e]
        model_name = self.model.model_name
        filename = "{model_name}_ep{ep}_{result_name}{result}".format(model_name=model_name, ep=ep, result_name=result_name, result=result)
        self.submit(self.model, filename)

    def run(self):
        running_epoch = self.epochs[self.running_i_e:]
        train_metric_logger = MetricLogger(delimiter="  ", writer=SummaryWriter(self.log_epoch_path + "train/"))
        valid_metric_logger = MetricLogger(delimiter="  ", writer=SummaryWriter(self.log_epoch_path + "valid/"))

        train_log_generator = train_metric_logger.log_every_epoch(running_epoch, self.epoch_print_freq, 'Train ')
        valid_log_generator = valid_metric_logger.log_every_epoch(running_epoch, self.epoch_print_freq, 'Test ')
        self.train_id, self.valid_id = self.get_kfold()

        for _, (e, _) in enumerate(zip(train_log_generator, valid_log_generator)):
            print("\n")
            self.optimizer.lr = self.learning_rate[self.running_i_e]
            np.random.shuffle(self.train_id)
            np.random.shuffle(self.valid_id)
            train_dataset, valid_dataset = Subset(deepcopy(self.dataset), self.train_id), Subset(
                deepcopy(self.dataset), self.valid_id)
            train_dataset.dataset.transform = self.train_transform
            valid_dataset.dataset.transform = self.valid_transform
            self.train_loader = DataLoader(train_dataset, **self.train_dataload_params)
            self.evaluate_train_loader = DataLoader(train_dataset, **self.valid_dataload_params)
            self.valid_loader = DataLoader(valid_dataset, **self.valid_dataload_params)

            train_one_epoch(self.model, self.criterion, self.train_loader, self.device, e, self.optimizer,
                            self.batch_print_freq, self.log_batch_path)
            train_result = evaluate(self.model, self.criterion, self.evaluate_train_loader, self.device)
            valid_result = evaluate(self.model, self.criterion, self.valid_loader, self.device)

            train_metric_logger.update(**train_result)
            valid_metric_logger.update(**valid_result)

            if (valid_result["mean_auc"] > self.max_score and valid_result["mean_auc"] > 0.986) or valid_result[
                "mean_auc"] > 0.99:
                self.max_score = valid_result["mean_auc"]
                self.save_model_checkpoint("mean_auc", valid_result["mean_auc"])

            self.running_i_e += 1

            if e % self.dump_freq == 0:
                self.dump()
        self.save(0)


class TaskTrainInvalid(TaskTrain):

    def __init__(self, *args, **kwargs):
        super(TaskTrainInvalid, self).__init__(*args, **kwargs)
        self.save_loss_max = 0.1

    def run(self):
        train_metric_logger = MetricLogger(delimiter="  ", writer=SummaryWriter(self.log_epoch_path + "running_train/"))

        running_epoch = self.epochs[self.running_i_e:]
        train_log_generator = train_metric_logger.log_every_epoch(running_epoch, self.epoch_print_freq, 'Running Train ')

        for _, e in enumerate(train_log_generator):
            print("\n")
            self.optimizer.lr = self.learning_rate[self.running_i_e]

            train_dataset = deepcopy(self.dataset)
            train_dataset.transform = self.train_transform
            self.train_dataload_params["shuffle"] = True
            self.train_loader = DataLoader(train_dataset, **self.train_dataload_params)

            running_train_result = train_one_epoch(self.model, self.criterion, self.train_loader, self.device, e, self.optimizer, self.batch_print_freq, self.log_batch_path)
            train_metric_logger.update(**running_train_result)

            if e in self.submit_checkpoint_list:
                self.submit_checkpoint("loss", running_train_result["loss"])

            self.running_i_e += 1

            if e % self.dump_freq == 0:
                    self.dump()

        self.save(0)

