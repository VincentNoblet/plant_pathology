import config
import pandas as pd
import numpy as np
from pathlib import Path
import os


class TrackTrainingMetadataManager:
    def __init__(self, training_metadata_path=config.STORAGE_PATH, create_first=False):
        self.training_metadata_filename = "training_metadata.xlsx"
        self.training_metadata_path = training_metadata_path + self.training_metadata_filename
        if create_first:
            if Path(self.training_metadata_path).exists():
                print("File training_metadata.xlsx exists already")
            else:
                self.save_metadataframe(pd.DataFrame([]))

    def load_metadaframe(self):
        return pd.read_excel(self.training_metadata_path)

    def load_last(self):
        metadata_frame = self.load_metadaframe()
        id_max = metadata_frame["id"].max()
        return metadata_frame[metadata_frame["id"] == id_max]

    def save_metadataframe(self, frame):
        frame.to_excel(self.training_metadata_path, index=False)

    def add_line(self, line):
        metadata_frame = self.load_metadaframe()
        id_max = metadata_frame["id"].max()
        frame_add = pd.DataFrame(line)
        frame_add["id"] = id_max + 1
        if set(frame_add.columns) == set(metadata_frame.columns):
            metadata_frame = pd.concat([metadata_frame, frame_add])
        else:
            raise TypeError("columns shoud be " + str(list(metadata_frame.columns)))
        self.save_metadataframe(metadata_frame)

    def add_column(self, column_name):
        metadata_frame = self.load_metadaframe()
        if column_name not in metadata_frame.columns:
            metadata_frame[column_name] = np.nan
        else:
            print(column_name + " is already in metadataframe")
        self.save_metadataframe(metadata_frame)

    def remove_metadataframe(self):
        if input("are you sure? (y/n)") != "y":
            pass
        else:
            os.remove(self.training_metadata_path)
