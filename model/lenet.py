from torch import nn
import numpy as np
from torch import functional as F


class LeNet(nn.Module):
    def __init__(self, height, width):
        super().__init__()
        self.running_height_width = np.array([height, width])
        self.conv1 = nn.Conv2d(3, 30, (5, 5), 1)
        self.running_height_width = self.output_size_conv(self.running_height_width, np.array(self.conv1.kernel_size))
        self.max_pool1 = nn.MaxPool2d(2, 2)
        self.running_height_width = self.output_size_pool(self.running_height_width, np.array(self.max_pool1.kernel_size))
        self.conv2 = nn.Conv2d(30, 60, (5, 5), 1)
        self.running_height_width = self.output_size_conv(self.running_height_width, np.array(self.conv2.kernel_size))
        self.max_pool2 = nn.MaxPool2d(2, 2)
        self.running_height_width = self.output_size_pool(self.running_height_width, np.array(self.max_pool2.kernel_size))
        self.input_size_dense = self.conv2.out_channels * self.running_height_width[0] * self.running_height_width[1]
        self.fc1 = nn.Linear(self.input_size_dense, 50)
        self.dropout1 = nn.Dropout(0.5)
        self.fc2 = nn.Linear(50, 10)
        self.dropout2 = nn.Dropout(0.5)
        self.fc3 = nn.Linear(10, 4)
    @staticmethod
    def output_size_conv(W_H, K):
        P = np.array([0, 0])
        S = np.array([1, 1])
        return 1 + (W_H - K + 2 * P) // S
    @staticmethod
    def output_size_pool(W_H, K):
        P = np.array([0, 0])
        S = np.array([2, 2])
        return 1 + (W_H - K + 2 * P) // S

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.max_pool1(x)
        x = F.relu(self.conv2(x))
        x = self.max_pool2(x)
        x = x.view(-1, self.input_size_dense)
        x = F.relu(self.fc1(x))
        x = self.dropout1(x)
        x = F.relu(self.fc2(x))
        x = self.dropout2(x)
        x = self.fc3(x)
        return x